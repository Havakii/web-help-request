let apiCall = function () {

    let urlUser = `https://web-help-request-api.herokuapp.com/users`;
    let urlTicket = `https://web-help-request-api.herokuapp.com/tickets`;

            fetch(urlUser).then((response) =>
                    response.json().then((dataUser) => {
                        for (var i = 0; i < dataUser.data.length; i+=1) {
                            // console.log(dataUser.data[i].username);

                            const option = document.createElement("option");
                            option.innerHTML = dataUser.data[i].username;
                            option.value = dataUser.data[i].id;
                            document.getElementById("sel").appendChild(option);
                            // console.log(dataUser);
                        } 

                        fetch(urlTicket).then((reponse) =>
                                reponse.json().then((dataTicket) => {
                                for (var i = 0; i < dataTicket.data.length; i+=1) {
                                // console.log(dataTicket)
                                // console.log(dataTicket.data[i].subject);

                                var tbody = document.getElementById('tbody');
                                tbody.innerHTML += `<tr">
                                <td class="text-center p-4 border-b-2 border-x-2 border-amber-400">${dataTicket.data[i].id}</td>
                                <td class="text-center p-4 border-b-2 border-x-2 border-amber-400">${dataUser.data[dataTicket.data[i].users_id - 1].username}</td>
                                <td class="text-center p-4 border-b-2 border-x-2 border-amber-400 ">${dataTicket.data[i].subject}</td>
                                <td class="text-center p-4 border-b-2 border-x-2 border-amber-400">${dataTicket.data[i].date}</td>
                                <td class="border-b-2 border-x-2 border-amber-400">
                                    <button id="patch"class="bg-sky-900 text-white rounded-lg p-1 mx-3">Passer mon tour</button>
                                </td>
                                </tr>`;     
                                } 
                 
                            })
                        );
            
                const patch = document.getElementById('next');
                patch.addEventListener('submit', function(e) {
                    e.preventDefault();
                    fetch(urlTicket).then((reponse) =>
                        reponse.json().then((dataTicket) => {

                            id = dataTicket.data[0].id
                            fetch(urlTicket + "/"+ id, {
                                method: 'PATCH',
                                body: JSON.stringify ({
                            })

                            }).then((response) => {
                            response.json().then((response) => {
                                console.log(response);
                            })
                            }).catch(err => {
                            console.error(err)
                            })
                        })
                    );
                })
            })
        ); 
    
    const myForm = document.getElementById("ticket-post");
    myForm.addEventListener("submit", function() {
        const ticket = document.getElementById("ticket");
        // console.log(ticket.value)
        const select = document.getElementById("sel")
        // console.log(select.value)
        // e.preventDefault();
    
        // const formData = new FormData(this);
        // console.log(formData);
        fetch(urlTicket, { 
            method: 'post',
            // headers: {
            //     "Content-Type": "application/x-www-form-urlencoded",
            // },
            body: new URLSearchParams({ 
                subject: ticket.value,
                userId: select.value
            })
        
        }).then(function(response) {
            return response.json();
        }).then(function(json) {
            // console.log(json);
        }).catch (function(error){
            // console.log(error);
        })
    }); 

};

apiCall();

